package com.company;

import java.util.Scanner;

public class tictactoe {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        // Buat array dengan nama board untuk menyimpan langkah pemain
        char[][] board = {{' ', ' ', ' '},
                          {' ', ' ', ' '},
                          {' ', ' ', ' '}};

        // Set a boolean for the player to keep track of turn
        boolean isPlayerA = true;

        // Keep track if game ended
        boolean gameEnded = false;

        while (!gameEnded) {
            System.out.println("----------------------------------------------------------------------");
            System.out.println("                          PERMAINAN PING DOL                          ");
            System.out.println("----------------------------------------------------------------------");

            drawBoard(board);

            // Keep track of player symbol
            char symbol = ' ';

            if (isPlayerA) {
                symbol = 'x';
            } else {
                symbol = 'o';
            }

            // Print out the player's turn
            if (isPlayerA) {
                System.out.println("Pemain A mulai -> Masukkan baris dan kolom");
            } else {
                System.out.println("Pemain B mulai -> Masukkan baris dan kolom");
            }

            // Get the row and col input
            int col = 0;
            int row = 0;

            System.out.print("Baris:"); row = in.nextInt();
            System.out.print("Kolom:"); col = in.nextInt();

            // Setting player symbbol postition on the board
            board[row - 1][col - 1] = symbol;

            // Check if the player's won
            if (hasWon(board) == 'x') {
                drawBoard(board);
                System.out.println("Permainan selesai .....");
                gameEnded = true;
            } else if (hasWon(board) == 'o') {
                drawBoard(board);
                System.out.println("Permainan selesai .....");
                gameEnded = true;
            } else {
                isPlayerA = !isPlayerA;
            }

        }

    }

    public static char hasWon(char[][] board) {
        // Col
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ') {
                return board[i][0];
            }
        }

        // Row
        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != ' ') {
                return board[0][j];
            }
        }

        // Diagonal
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ') {
            return board[0][0];
        }

        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != ' ') {
            return board[0][2];
        }
        // Nobody has won yet
        return ' ';
    }

    public static void drawBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            System.out.println("* * * * * * * * * * * * * * * *");
            System.out.println("*         *         *         *");
            System.out.println("*    " + board[i][0] + "    *    " + board[i][1] + "    *    " + board[i][2] + "    *");
            System.out.println("*         *         *         *");
        }
        System.out.println("* * * * * * * * * * * * * * * *");
    }
}